<%-- 
    Document   : index
    Created on : 31-03-2020, 19:37:05
    Author     : jarqu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>-->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-css"/>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <meta name = "viewport" content="width=device-width, initial-scale=1.0">
        <title>Simulador de Crédito ::AthorZ Bank::</title>
    </head>
    <body class="bg-dark">
        <div class="container">
            <h2 class="text-center">The AthorZ Bank</h2>
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-lg-6 pb-5">
                    <form action="controller" method="POST">
                        <div class="card border-primary rounded-0">
                            <div class="card-header p-0">
                                <div class="bg-info text-white text-center py-2">
                                    <h3><i class="fa fa-bank"></i> Simula tu credito</h3>
                                    <p class="m-0">Puedes elegir tu tasa de interés preferencial</p>
                                </div>
                            </div>
                            <div class="card-body p-3">

                                <!--Body-->
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-user text-info"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre y Apellido" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-usd text-info"></i></div>
                                        </div>
                                        <input type="number" class="form-control" id="capital" name="capital" placeholder="Monto a Solicitar" min="100000" step="50000" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-pie-chart text-info"></i></div>
                                        </div>
                                        <input type="number" class="form-control" id="anios" name="anios" min="2" max="10" placeholder="Años" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-percent text-info"></i></div>
                                        </div>
                                        <input type="number" class="form-control" id="tasa" name="tasa" type="number" placeholder="Tasa preferencial a solicitar" min="5" max="15" step="5" required>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input type="submit" value="Enviar" class="btn btn-info btn-block rounded-0 py-2" style="font-weight: bold">
                                </div>
                                <br>
                                <div ><input class="btn btn-info btn-block rounded-0 py-2" type="reset" value="Borrar"></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
                    <div class="text-light" style="text-align: center; margin-top: 10px;"><p>Programado por : Jorge Arqueros Reyes - Evaluacion 01 CIISA - 30 de Marzo del 2020</p></div>
                    <!--
                        <h2> Formato Simulación </h2>
                        <p> Hola, por favor introduce la información </p>
                        <form action="controller" method="POST">
                            <table cellspacing="3" cellpadding="3" border="1" >
                                <tr>
                                    <td align="right"> Nombre: </td>
                                    <td> <input type="text" name="nombre" placeholder="András Arató"></td> 
                                </tr>  
                                <tr>
                                    <td align="right"> Monto a solicitar: </td>    
                                    <td><input type="text" name="capital" placeholder="ej. $100000"></td> 
                                </tr>
                                <tr>
                                    <td align="right"> Porcentaje Tasa: </td>    
                                    <td> <input type="number" name="tasa" min="5" max="15" placeholder="5">% </td> 
                                </tr>  
                                <tr>
                                    <td align="right"> Años deposito: </td>    
                                    <td> <input type="number" name="anios" min="2" max="8" placeholder="ej. 3"> </td> 
                                </tr>  
                            </table>
                    
                            <input type="reset" value="Borrar">
                            <input type="submit" value="Enviar">
                        </form>-->
                    </body>
                    </html>
