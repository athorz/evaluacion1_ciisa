/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;

/**
 *
 * @author jarqu
 */
public class CalcIntSimp {
    
    public static double getInteres( String capitalStr, String aniosStr, String tasaStr){
        
        double capitalNum = Double.parseDouble(capitalStr);
        int aniosNum = Integer.parseInt(aniosStr);
        int tasaNum = Integer.parseInt(tasaStr);
        
        double capitalInteres;
        
        capitalInteres = (capitalNum * (tasaNum * 0.01) * aniosNum);
        
        return capitalInteres;
        
    }
    
}
