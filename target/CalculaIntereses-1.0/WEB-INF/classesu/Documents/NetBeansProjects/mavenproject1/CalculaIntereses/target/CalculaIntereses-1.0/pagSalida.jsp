<%-- 
    Document   : pagSalida
    Created on : 31-03-2020, 19:51:52
    Author     : jarqu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>-->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-css"/>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <meta name = "viewport" content="width=device-width, initial-scale=1.0">
        <title>Resultados Simulacion ::AthorZ Bank::</title>
    </head>

    <body class="bg-dark">
        <%
            String nombre = (String) request.getAttribute("nombre");
            String ahorro = (String) request.getAttribute("capital");
            double interesCalc = (double) request.getAttribute("interesCalc");
            String anios = (String) request.getAttribute("anios");
            String tasa = (String) request.getAttribute("tasa");
        %>
        <div class="container">
            <h2 class="text-center">The AthorZ Bank</h2>
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 col-lg-6 pb-5">
                    <div class="card border-primary rounded-0">
                        <div class="card-header p-0">
                            <div class="bg-info text-white text-center py-2">
                                <h3><i class="fa fa-edit"></i> Resultado de Simulación</h3>
                                <p class="m-0">Revisa nuestra simulación si estás de acuerdo</p>
                            </div>
                        </div>
                        <div class="card-body p-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Intereses Calculados</th>
                                        <th scope="col">$<%= interesCalc%></th>

                                    </tr>
                                    <tr>
                                        <th scope="col">Crédito + Intereses</th>
                                        <th scope="col">$<%= Double.parseDouble(ahorro) + interesCalc%></th>

                                    </tr>
                                </thead>
                                <!--Body-->
                                <tbody>
                                    <tr>
                                        <th scope="row">Cliente</th>
                                        <td><%= nombre%></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Monto Solicitado</th>
                                        <td>$<%= ahorro%></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Tasa Solicitada</th>
                                        <td><%= tasa%>%</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Años Cuota</th>
                                        <td><%= anios%></td>
                                    </tr>
                                </tbody>
                            </table>
                            <form action="index.jsp" method="post">
                                <input class="btn btn-info btn-block rounded-0 py-2" type="submit" value="Volver a simular">
                            </form>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="text-light" style="text-align: center; margin-top: 10px;"><p>Programado por : Jorge Arqueros Reyes - Evaluacion 01 CIISA - 30 de Marzo del 2020</p></div>
        <!--
                <h1>Estimado(a) <%= nombre%>: </h1>
                <h2> Resultados de Simulación </h2>
                <h3> Aqui se despliegan los datos que se recibieron...</h2>
                <p> Esta es nuestra simulación de intereses en tus ahorros: s</p>
        
                <table cellspacing="3" cellpadding="3" border="1" >
        
                    <tr>
                        <td align="right"> Nombre Cliente: </td>
                        <td> <%= nombre%> </td> 
                    </tr>  
                    <tr>
                        <td align="right"> Monto Crédito: </td>    
                        <td> $<%= ahorro%> </td> 
                    </tr>
                    <tr> 
                        <td align="right"> Tasa Preferencial Solicitada: </td>    
                        <td> <%= tasa%>% </td> 
                    </tr>  
                    <tr> 
                        <td align="right"> Años Cuota: </td>    
                        <td> <%= anios%> </td> 
                    </tr>  
                </table>
        
                <table cellspacing="3" cellpadding="3" border="2">
                    <tr>
                        <td align="right"> Intereses calculados: </td>
                        <td> $<%= interesCalc%> </td> 
                    </tr>  
                    <tr>
                        <td align="right"> Tu ahorro + Intereses: </td>    
                        <td> $<%= Double.parseDouble(ahorro) + interesCalc%> </td> 
                    </tr>
                </table>
        -->
                <!--<p>Tu Ahorro en <%= anios%> años con nuestro interes es de: </p>
                <p><%= interesCalc%></p>-->


    </body>
</html>
